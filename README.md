# README #

### Setup Instructions ###

* Clone repository in the DesktopModules folder of your DNN website 
* Rename cloned folder to "FacebookModule"
* Log into your DNN website dashboard, select "Host/Extensions"
* Click "Install Extension Wizard" and select the install file *(your website directory/DesktopModules/FacebookModule/install/FacebookModule_01.00.00_Install.zip)*
* After install is complete, create a new page and add the FacebookModule as a new module on the page
* Close the editor, and view the page to log into Facebook and view your information and publish new posts

### Known Bugs ###

None at the moment

### Contact ###
George Olson at georgeolson92@gmail.com